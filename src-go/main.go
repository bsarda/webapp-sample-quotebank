package main

import (
	// basics
	"fmt"
	"log"
	"errors"
	"strings"
	"encoding/json"
	"os"
	// mssql driver
	_ "github.com/denisenkom/go-mssqldb"
	// postgres driver
	_ "github.com/lib/pq"
	// mysql driver
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	// http-web requirements
	"strconv"
	"net/http"
	"github.com/gin-gonic/gin"
)

type Quote struct {
	ID     int	`json:"id"`
	Quote  string	`json:"quote" binding:"required"`
	Likes  int	`json:"likes"`
	Author string   `json:"author"`
	Source string	`json:"source"`
}
type QuoteReq struct {
	Quote  string	`json:"quote" binding:"required"`
	Author string   `json:"author" binding:"required"`
	Source string	`json:"source" binding:"required"`
}

type database struct {
	DB    	*sql.DB
	Table	string
	Server	string
	Brand	string
}

var quotes = []Quote{}

func main() {
	LaunchWebServer(FetchServerPortFromEnv())
}

func LaunchWebServer(port int){   
	// Set the default router
	router := gin.Default()
	// Setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/ping", func(c *gin.Context) {
			c.JSON( http.StatusOK, gin.H {"message": "pong",} )
		})
		api.GET("/quotes", GinQuoteHandler)
		api.OPTIONS("/quotes", GinQuoteHandler)
		api.POST("/quotes", GinQuoteAddHandler)
		api.POST("/quotes/like/:quoteID", GinQuoteLikeHandler)
		api.OPTIONS("/quotes/:quoteID", GinQuoteHandler)
		api.DELETE("/quotes/:quoteID", GinQuoteDelHandler)
	}
	// trust no proxies
	router.SetTrustedProxies(nil)
	// Start and run the server
	router.Run(":"+strconv.Itoa(port))
}

// ===== GIN router handlers =====

// basic options responder
func GinOptionsHandler(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
	c.AbortWithStatus(204)
}

// retrieve the list of quotes
func GinQuoteHandler(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	// add the cors headers
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

	// fetch quotes (on page init/refresh) in the global variable (because like reuse global)
	var err error
	quotes, err = FetchAllQuotesFromDB()
	if err != nil {
		log.Fatal("Failed to fetch quotes from database: ", err.Error())
	}
	c.JSON(http.StatusOK, quotes)
}

// retrieve the list of quotes
func GinQuoteAddHandler(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	// add the cors headers
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
	// extract response to json
	var requestBody Quote
	if err := c.BindJSON(&requestBody); err != nil {
		c.AbortWithStatus(http.StatusNotAcceptable)
   	}
	AddQuoteToDB(requestBody.Quote, requestBody.Author, requestBody.Source)
	c.JSON(http.StatusOK, quotes)
}

// retrieve the list of quotes
func GinQuoteDelHandler(c *gin.Context) {
	// add the cors headers
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
	// process
	if quoteId, err := strconv.Atoi(c.Param("quoteID")); err == nil {
		err := DelQuoteFromDB(int64(quoteId))
		if err != nil {
			fmt.Println("Error while deleting quote: ",err.Error())
		}
		// return a pointer to the updated list
		c.JSON(http.StatusOK, &quotes)
	} else {
		// quoteID is invalid
		c.AbortWithStatus(http.StatusNotFound)
	}
}

// push the like button
func GinQuoteLikeHandler(c *gin.Context) {
	if quoteId, err := strconv.Atoi(c.Param("quoteID")); err == nil {
		// find the index from the table
		indexQuote := -1
		for i, quot := range quotes {
			// log.Printf("i=%v , id=%v, quotes[i].ID=%v\n", i, quoteId, quot.ID)
			if quot.ID == quoteId {
				indexQuote = i
			}
		}
		// increment in the global variable before sql update
		quotes[indexQuote].Likes +=1
		// sql update
		// fetch quotes (on page init/refresh) in the global variable (because like reuse global)
		err := UpdateLikesToDB(quoteId, quotes[indexQuote].Likes)
		if err != nil {
			log.Println("Error while updating record: ",err.Error())
		}
		// return a pointer to the updated list
		c.JSON(http.StatusOK, &quotes)
	} else {
		// quoteID is invalid
		c.AbortWithStatus(http.StatusNotFound)
	}
}

// ===== backend functions for "quotes" app =====

// retrieve variables from os:env to be used for server port
func FetchServerPortFromEnv() (int){
	srvPort,err := strconv.Atoi(os.Getenv("SERVER_PORT"))
	if err != nil {
		log.Println("Failed to fetch port from config. Error: ", err.Error())
		// default port
		return 8280
	}
	return srvPort
}

// retrieve variables from os:env to be used for SQL connection
func FetchSQLConnectivityVarsFromEnv() (string, string, int, string, string, string, string){
	dbType := ""
	switch os.Getenv("DB_TYPE") {
	case "pg", "pgx", "postgres", "postgresql":
		dbType = "postgres"
	case "mysql", "my", "maria", "mariadb":
		dbType = "mysql"
	default:
		// defaults to mssql. other choices
		dbType = "mssql"
	}
	dbHost := os.Getenv("DB_HOST")
	dbPort,_ := strconv.Atoi(os.Getenv("DB_PORT"))
	dbName := os.Getenv("DB_NAME")
	dbTable := os.Getenv("DB_TABLE")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	return dbType, dbHost, dbPort, dbName, dbTable, dbUser, dbPass
}

// read all quotes from db (a classic and dirty readall, select*)
func FetchAllQuotesFromDB() ([]Quote, error) {
	dbType, server, port, dbname, table, username, password := FetchSQLConnectivityVarsFromEnv()
	// connect -- takes 500ms
	var db *database
	var err error

	switch dbType {
	case "postgres":
		db,err = pgxConnect(server, port, dbname, username, password)
	case "mysql":
		db,err = mysqlConnect(server, port, dbname, username, password)
	case "mssql":
		db,err = mssqlConnect(server, port, dbname, username, password)
	}
	defer db.Close()
	if err != nil {
		log.Fatal("Failed to connect. Error: ", err.Error())
		return nil, err
	}
	//	set the working table
	db.Table = table
	// read the records - takes 170ms
	records, err := db.ReadRecords()
	if err != nil {
		log.Fatal("Failed to fetch records. Error: ", err.Error())
		return nil, err
	}
	quotes := make([]Quote,len(records)-1)
	for i, row := range records {
		// skip first line as this is the columns title
		if i>0 {
			rowId,_ := strconv.Atoi(row[0])
			rowLikes,_ := strconv.Atoi(row[2])
			quotes[i-1] = Quote{ID: rowId, Quote: row[1], Likes: rowLikes, Author: row[3], Source: row[4]}
		}
	}
	return quotes, nil
}

// update the like counter for the quote
func UpdateLikesToDB(quoteId int, likes int) error {
	dbType, server, port, dbname, table, username, password := FetchSQLConnectivityVarsFromEnv()
	// connect -- takes 500ms
	var db *database
	var err error
	switch dbType {
	case "postgres":
		db,err = pgxConnect(server, port, dbname, username, password)
	case "mysql":
		db,err = mysqlConnect(server, port, dbname, username, password)
	case "mssql":
		db,err = mssqlConnect(server, port, dbname, username, password)
	}
	defer db.Close()
	if err != nil {
		log.Fatal("Failed to connect. Error: ", err.Error())
		return err
	}
	//	set the working table
	db.Table = table
	db.UpdateRecord( quoteId, map[string]interface{}{"Likes": likes} )
	return nil
}

// add a new quote
func AddQuoteToDB(quote string, author string, source string) (int64, error) {
	dbType, server, port, dbname, table, username, password := FetchSQLConnectivityVarsFromEnv()
	// connect -- takes 500ms
	var db *database
	var err error
	rec2create := make(map[string]string)
	rec2create["Quote"] = quote
	rec2create["Author"] = author
	rec2create["Source"] = source

	switch dbType {
	case "postgres":
		db,err = pgxConnect(server, port, dbname, username, password)
		// works the same than maria/mysql
		//	set the working table
		db.Table = table
		return db.pgxCreateRecord(rec2create)
	case "mysql":
		db,err = mysqlConnect(server, port, dbname, username, password)
		//	set the working table
		db.Table = table
		return db.mysqlCreateRecord(rec2create)
	case "mssql":
		db,err = mssqlConnect(server, port, dbname, username, password)
		//	set the working table
		db.Table = table
		return db.mssqlCreateRecord(rec2create)
	}
	defer db.Close()
	if err != nil {
		log.Fatal("Failed to connect. Error: ", err.Error())
		return 0, err
	}
	return 0, errors.New("Unknown Error")
}

// add a new quote
func DelQuoteFromDB(quoteId int64) error {
	dbType, server, port, dbname, table, username, password := FetchSQLConnectivityVarsFromEnv()
	// connect -- takes 500ms
	var db *database
	var err error
	switch dbType {
	case "postgres":
		db,err = pgxConnect(server, port, dbname, username, password)
	case "mysql":
		db,err = mysqlConnect(server, port, dbname, username, password)
	case "mssql":
		db,err = mssqlConnect(server, port, dbname, username, password)
	}
	defer db.Close()
	if err != nil {
		log.Fatal("Failed to connect. Error: ", err.Error())
		return err
	}
	db.Table = table
	return db.DeleteRecord(quoteId)
}

// ===== generic functions =====

// connect to db
func mysqlConnect(server string, port int, dbname string, username string, password string) (*database, error) {
	if (server == "" || dbname == "" || username == "" || password == "") {
		return nil, errors.New("Invalid inputs")
	}
	if (port == 0 || port < 0 || port > 65535) {
		port = 3306
	}
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
        username, password, server, port, dbname)

	// Create connection pool
    db, err := sql.Open("mysql", connectionString)
	if err != nil {
        log.Fatal("Error creating connection pool: ", err.Error())
		return nil, err
    }
	// return object
	sqlo := database{
		DB: db,
		Server: server,
		Table: "demo",	// set a default table
	}
	err = sqlo.IsAlive()
	if err != nil {
		log.Fatal("Database is not connected: ", err.Error())
		return nil, err
	}
	return &sqlo, nil
}
func pgxConnect(server string, port int, dbname string, username string, password string) (*database, error) {
	if (server == "" || dbname == "" || username == "" || password == "") {
		return nil, errors.New("Invalid inputs")
	}
	if (port == 0 || port < 0 || port > 65535) {
		port = 5432
	}
	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%d/%s",
        username, password, server, port, dbname)

	// Create connection pool
    db, err := sql.Open("postgres", connectionString)
	if err != nil {
        log.Fatal("Error creating connection pool: ", err.Error())
		return nil, err
    }
	// return object
	sqlo := database{
		DB: db,
		Server: server,
		Table: "demo",	// set a default table
	}
	err = sqlo.IsAlive()
	if err != nil {
		log.Fatal("Database is not connected: ", err.Error())
		return nil, err
	}
	return &sqlo, nil
}
func mssqlConnect(server string, port int, dbname string, username string, password string) (*database, error) {
	if (server == "" || dbname == "" || username == "" || password == "") {
		return nil, errors.New("Invalid inputs")
	}
	if (port == 0 || port < 0 || port > 65535) {
		port = 1433
	}
	connectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;",
        server, username, password, port, dbname)
	// Create connection pool
    db, err := sql.Open("sqlserver", connectionString)
	if err != nil {
        log.Fatal("Error creating connection pool: ", err.Error())
		return nil, err
    }
	// return object
	sqlo := database{
		DB: db,
		Server: server,
		Table: "demo",	// set a default table
	}
	err = sqlo.IsAlive()
	if err != nil {
		log.Fatal("Database is not connected: ", err.Error())
		return nil, err
	}
	return &sqlo, nil
}

// closing database MSSQL/MySQL
func (db *database) Close() {
	db.DB.Close()
}

// check if database is alive MSSQL/MySQL
func (db *database) IsAlive() error{
	if db == nil {
		return errors.New("No connection active, please first connect")
	}
	if db.DB == nil {
		return errors.New("No connection active, please first connect")
	}
	// ping. it tooks approx 500ms!
	err:= db.DB.Ping()
    if err != nil {
        return err
    }
	return nil
}

// read all (select *) MSSQL/MySQL
func (db *database) ReadRecords() ([][]string, error){
	// check if db is alive
	err := db.IsAlive()
	if err != nil {
		return nil, err
	}
    tsql := fmt.Sprintf("SELECT * FROM %s;",db.Table)
    // Execute query
    rows, err := db.DB.Query(tsql)
    if err != nil {
        return nil, err
    }
    defer rows.Close()
	// get the fields and return
	return _rowsToStrings(rows), nil
}

// read by filters
func (db *database) FindRecordsByFields(kv map[string]string, strictMatch bool) ([][]string, error) {
	// check if db is alive
	err := db.IsAlive()
	if err != nil {
		return nil, err
	}
	// init query line
	queryline := ""
	for key, value := range kv {
		if strictMatch == true {
			queryline = fmt.Sprintf("%s lower(%s) LIKE '%%%s%%' AND", queryline, key, value)
		} else {
			queryline = fmt.Sprintf("%s lower(%s) = '%s' AND", queryline, key, value)
		}
	}
	tsql := fmt.Sprintf("SELECT * FROM %s WHERE%s", db.Table, strings.TrimRight(queryline, " AND"))
    // Execute query
    rows, err := db.DB.Query(tsql)
	defer rows.Close()
    if err != nil {
        return nil, err
    }
	// get the fields and return
	return _rowsToStrings(rows), nil
}

// create an entry
func (db *database) mysqlCreateRecord(rec map[string]string) (int64, error){
	// check if db is alive
	err := db.IsAlive()
	if err != nil {
		return -1, err
	}
	// init query line
	var recordCols []string
	var recordVals []string
	for key, value := range rec {
		recordCols = append(recordCols, strings.Replace(key,"'", "''", -1) )
		recordVals = append(recordVals, "'"+strings.Replace(value,"'", "''", -1)+"'" )
	}
	fmt.Println("HERE: ", db.Table)
	// mysql type query
	tsql := fmt.Sprintf("INSERT INTO %s(%s) VALUES(%s);", db.Table, strings.Join(recordCols, ", "), strings.Join(recordVals, ", "))
	fmt.Println("tsql: ", tsql)
	result, err := db.DB.Exec(tsql)

	insertId, _ := result.LastInsertId()
	return insertId, nil
}
func (db *database) pgxCreateRecord(rec map[string]string) (int64, error){
	// check if db is alive
	err := db.IsAlive()
	if err != nil {
		return -1, err
	}
	// init query line
	var recordCols []string
	var recordVals []string
	for key, value := range rec {
		recordCols = append(recordCols, strings.Replace(key,"'", "''", -1) )
		recordVals = append(recordVals, "'"+strings.Replace(value,"'", "''", -1)+"'" )
	}
	// mysql type query
	tsql := fmt.Sprintf("INSERT INTO %s(%s) VALUES(%s) RETURNING Id;", db.Table, strings.Join(recordCols, ", "), strings.Join(recordVals, ", "))
	// pq driver does not support lqstInsertId() - https://pkg.go.dev/github.com/lib/pq#section-readme
	var insertId int64
	errQuery := db.DB.QueryRow(tsql).Scan(&insertId) 
	return insertId, errQuery
}
func (db *database) mssqlCreateRecord(rec map[string]string) (int64, error){
	// check if db is alive
	err := db.IsAlive()
	if err != nil {
		return -1, err
	}
	// init query line
	var recordCols []string
	var recordVals []string
	for key, value := range rec {
		recordCols = append(recordCols, strings.Replace(key,"'", "''", -1) )
		recordVals = append(recordVals, "'"+strings.Replace(value,"'", "''", -1)+"'" )
	}
	// sqlserver type query
	tsql := fmt.Sprintf("INSERT INTO %s(%s) VALUES(%s); SELECT ID = convert(bigint, SCOPE_IDENTITY());", db.Table, strings.Join(recordCols, ", "), strings.Join(recordVals, ", "))
	rows, err := db.DB.Query(tsql)
	defer rows.Close()
	if err != nil {
		return -2, err
	}
	var insertId int64
	for rows.Next() {
		rows.Scan(&insertId)
	}
	return insertId, nil
}

// update an entry MSSQL/MySQL
func (db *database) UpdateRecord(id int, rec map[string]interface{}) error {
	// check if db is alive
	errA := db.IsAlive()
	if errA != nil {
		return errA
	}

	fmt.Printf("updating record id %d with rec = %s\n", id, rec)
	fmt.Println("dbTable = ", db.Table)

	// init query line
	var updateValuesArray []string
	for key, value := range rec {
		// if reflect.Type
		if fmt.Sprintf("%T", value) == "int" {
			updateValuesArray = append(updateValuesArray, fmt.Sprintf("%s=%v",key,value))
		} else {
			// considering it's a string
			updateValuesArray = append(updateValuesArray, fmt.Sprintf("%s=N'%s'",key,value))
		}	
	}
	tsql := fmt.Sprintf("UPDATE %s SET %s WHERE Id=%v", db.Table, strings.Join(updateValuesArray, ", "), id)
	_, err := db.DB.Query(tsql)
	return err
}

// deleting an entry MSSQL/MySQL/Postgres
func (db *database) DeleteRecord(recId int64) error{
	// check if db is alive
	err := db.IsAlive()
	if err != nil {
		return err
	}
	tsql := fmt.Sprintf("DELETE FROM %s WHERE Id = %v;", db.Table, recId)
	rows, err := db.DB.Query(tsql)
	defer rows.Close()
	if err != nil {
		return err
	}
	return nil
}

// ===== internal and test functions =====

// for tests mainly, pretty prints json data from struct
func PrettyStruct(data interface{}) (string, error) {
    val, err := json.MarshalIndent(data, "", "  ")
    if err != nil {
        return "", err
    }
    return string(val), nil
}

// internal func to extract raws to [][]string
func _rowsToStrings(rows *sql.Rows) [][]string {
	cols, err := rows.Columns()
	if err != nil {
		panic(err)
	}
	pretty := [][]string{cols}
	results := make([]interface{}, len(cols))
	for i := range results {
		results[i] = new(interface{})
	}
	for rows.Next() {
		if err := rows.Scan(results[:]...); err != nil {
			panic(err)
		}
		cur := make([]string, len(cols))
		for i := range results {
			val := *results[i].(*interface{})
			var str string
			if val == nil {
				str = "NULL"
			} else {
				switch v := val.(type) {
				case []byte:
					str = string(v)
				default:
					str = fmt.Sprintf("%v", v)
				}
			}
			cur[i] = str
		}
		pretty = append(pretty, cur)
	}
	return pretty
}
