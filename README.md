# webapp-sample-go

## Introduction

Sample of multi-tier app with web frontend and Go middleware, connecting to a backend MSSQL DB

Inspired from the posts:
[Go gin and react.js](https://www.freecodecamp.org/news/how-to-build-a-web-app-with-go-gin-and-react-cffdc473576/)
[SQL in Go](https://www.sohamkamani.com/golang/sql-database/)
[webapp in go](https://hackernoon.com/hello-world-in-golang-how-to-develop-a-simple-web-app-in-go-2l39316u)

There are multiple "versions" of the application:

- basic binaries and web files, to be executed as a service and with apache2
  - can be hosted on a single VM for front+middleware, or separate.
- containerized binaries and web files, to be executed as container and with apache2
  - can be used with Amazon ECS for container and an instance with apache2 installed
  - or can be used with ECS and static content from S3/CloudFront

Roadshow is:

- there is a VM which hosts the binary+web and another VM which host the database.
  - web can be also hosted on static, CDN or container
  - binary could be container or serverless, and database RDS or Aurora.

## Prerequisites

You will need the following:

- a dev environment
  - with Go installed and the modules
  - with Docker installed
- a remote workstation (or your laptop)
  - used to connect to database and list records with the UI
- an Amazon RDS database using MSSQL Express

### Install go

on ubuntu 20.04:

```bash
sudo apt update && sudo apt upgrade -y
sudo apt install -y curl wget
wget -c https://go.dev/dl/go1.21.4.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
```

add to path, edit `/etc/profile`

```bash
sudo vi /etc/profile
```

add at the end:  
`export PATH=$PATH:/usr/local/go/bin`  
and then source the file:

```bash
source /etc/profile
```

finish go prepare

```bash
go version
mkdir ~/go
```

### Install go modules and prepare app

First create the folder for project:

```bash
mkdir -p ~/go/src/myapp
```

with recent versions, you'll need to create the go mod

```bash
go mod init
go mod tidy
go run main.go
```

### Remote workstation

Deploy a remote workstation, or use your laptop.  
Install **DBeaver** on the remote workstation.  
If there is a Java issue when trying to open connection, edit `dbeaver.ini`, remove the line `-Djavax.net.ssl.trustStoreType=WINDOWS-ROOT` and restart DBeaver.

## Prepare environment

### Create Database

create the database "db". Example for MS SQL

```sql
USE master;
CREATE DATABASE 'db'; 
```

### Create Table

create the table "Quotes" in the database "db".

for MS SQL:

```sql
CREATE TABLE db.dbo.Quotes (
  Id       INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
  Quote    NVARCHAR(1000),
  Likes    INT DEFAULT(0),
  Author   NVARCHAR(255),
  Source   NVARCHAR(255),
);
GO
```

for MySQL/MariaDB:

```sql
CREATE TABLE db.Quotes (
  Id      INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Quote   NVARCHAR(1000),
  Likes   INT DEFAULT 0,
  Author  NVARCHAR(255),
  Source  NVARCHAR(255)
);
```

for PostgreSQL:

```sql
CREATE TABLE Quotes(
  Id serial primary key,
  Quote varchar,
  Likes int not null default 0,
  Author varchar,
  Source varchar
);
```

### Add records

```sql
INSERT INTO db.dbo.Quotes (Quote, Author, Source) VALUES
  (N'Yesterday is history, tomorrow is a mystery, and today is a gift... that’s why they call it the present', N'Master Oogway',N'Kung Fu Panda'),
  (N'Your story may not have such a happy beginning but that does not make you who you are, it is the rest of it – who you choose to be', N'Soothsayer',N'Kung Fu Panda 2'),
  (N'Your mind is like this water, my friend. When it gets agitated, it becomes difficult to see. But if you allow it to settle, the answer becomes clear.', N'Master Oogway',N'Kung Fu Panda'),
  (N'You are the master of your destiny: No one and nothing can come in between you and your destiny except you. Take destiny by the horns and have fun.', N'Po',N'Kung Fu Panda'),
  (N'You don’t need to meditate for hours and hours to attain inner peace and enlightenment. You need only see, feel, and act from the heart. Let the heart guide you to your peaceful enlightenment.', N'Po',N'Kung Fu Panda 2'),
  (N'Don’t push past memories deeper inside of yourself. Let those memories breathe and let old wounds heal.', N'Po',N'Kung Fu Panda'),
  (N'There are no accidents.', N'Master Oogway',N'Kung Fu Panda'),
  (N'Once I realized the problem was not you but within me. I found inner peace and was able to harness the flow of the universe.', N'Master Shifu',N'Kung Fu Panda 2'),
  (N'To make something special you just have to believe it’s special.', N'Mr Ping',N'Kung Fu Panda'),
  (N'There is just news. There is no good or bad.', N'Master Oogway',N'Kung Fu Panda'),
  (N'Time is an illusion, there is only the now.', N'Master Shifu',N'Kung Fu Panda 3'),
  (N'Get ready to dance... with danger!', N'Panda Mei Mei',N'Kung Fu Panda 3'),
  (N'Your real strength comes from being the best you you can be. Who are you? What are you good at? What makes you, you?', N'Po',N'Kung Fu Panda 3'),
  (N'There is always something more to learn. Even for a master.', N'Master Oogway',N'Kung Fu Panda 3'),
  (N'If you only do what you can do, you’ll never be more than you are now.', N'Master Shifu',N'Kung Fu Panda 3');
GO
```

### Set Environment variables

If this is intended to run directly the binary (and not the container), we need to set up locally the SQL connectivity informations
edit `/etc/profile` and add:

```bash
export DB_TYPE="<type>"
export DB_HOST="<your-rds-instance>.<aws-region>.rds.amazonaws.com"
export DB_PORT=1433
export DB_NAME="db"
export DB_TABLE="Quotes"
export DB_USER="admin"
export DB_PASS='<your-rds-password>'
```

type of db (as of now) can be postgres, mysql, mssql.

with example values:

RDS MariaDB :

```bash
export DB_TYPE="mysql"
export DB_HOST="db-maria.cm3abcdefghi.eu-west-1.rds.amazonaws.com"
export DB_PORT=3306
export DB_NAME="db"
export DB_TABLE="Quotes"
export DB_USER="admin"
export DB_PASS='mYP4$$'
```

Aurora PostgreSQL :

```bash
export DB_TYPE="postgres"
export DB_HOST="db-pgx.cluster-cm3abcdefghi.eu-west-1.rds.amazonaws.com"
export DB_PORT=5432
export DB_NAME="db"
export DB_TABLE="Quotes"
export DB_USER="postgres"
export DB_PASS='mYP4$$'
```

RDS Microsoft SQL Server :

```bash
export DB_TYPE="mssql"
export DB_HOST="goapp-db.c0itvbsynvuw.us-east-1.rds.amazonaws.com"
export DB_PORT=1433
export DB_NAME="db"
export DB_TABLE="Quotes"
export DB_USER="admin"
export DB_PASS='BlueTea9!'
```

### Prepare EC2/VMware instance as webserver

Deploy a Ubuntu or an Amazon Linux.  
Then, upgrade and deploy apache2:

Ubuntu:

```bash
sudo apt update && sudo apt upgrade -y
sudo apt install -y curl wget apache2
```

Amazon Linux:

```bash
sudo yum update -y
sudo yum install -y httpd.x86_64
sudo systemctl start httpd.service
sudo systemctl enable httpd.service
```

### Import the web files

Copy the files `index.html` and `styles.css` in `/var/www/html/`

replace the `localhost` with public ip

```bash
sudo sed -i "s|localhost|$(curl -s ipinfo.io/ip)|g" /var/www/html/index.html
```

or, if using an AWS EC2 instance

```bash
sudo sed -i "s|localhost|$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)|g" /var/www/html/index.html
```

## Prepare the binary

### Run directly the go file

Requires golang development environment.

```bash
go run go-src/main.go
```

### Build the go binary

Build from the machine it will be run (if linux, don't build on macOS...)

```bash
go build go-src/main.go
```

it generates the `main` binary file, ready to be executed.

### Build the container

Use the Dockerfile provided.

```bash
docker build -t quote-bank:1.0 .
```

then run it (with env vars) attached, first time to validate it works:

```bash
docker run --rm \
  -e MSSQL_DB_HOST="<your-rds-instance>.<aws-region>.rds.amazonaws.com" \
  -e MSSQL_DB_PORT=1433 \
  -e MSSQL_DB_NAME="db" \
  -e MSSQL_DB_TABLE="Quotes" \
  -e MSSQL_DB_USER="admin" \
  -e MSSQL_DB_PASS='<your-rds-password>' \
  -p 8280:8280 myapp
```

_of course, replace the DB_HOST variable with your instance name._

If everything's ok, run it detached and with restart.

```bash
docker run -d --restart=unless-stopped  \
  -e MSSQL_DB_HOST="goapp-db.c0itvbsynvuw.us-east-1.rds.amazonaws.com" \
  -e MSSQL_DB_PORT=1433 \
  -e MSSQL_DB_NAME="db" \
  -e MSSQL_DB_TABLE="Quotes" \
  -e MSSQL_DB_USER="admin" \
  -e MSSQL_DB_PASS='BlueTea9!' \
  -p 8280:8280 myapp
```

## License

Licensed under MIT License
