IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'quotebank')
BEGIN
  USE master;
  CREATE DATABASE quotebank;
END;
GO
CREATE TABLE [quotebank].[dbo].[studentXX] (
  Id      INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
  Quote   NVARCHAR(1000),
  Likes   INT DEFAULT(0),
  Author  NVARCHAR(255),
  Source  NVARCHAR(255),
);
INSERT INTO [quotebank].[dbo].[studentXX] (Quote, Author, Source) VALUES
  (N'Yesterday is history, tomorrow is a mystery, and today is a gift... that’s why they call it the present', N'Master Oogway',N'Kung Fu Panda'),
  (N'Your story may not have such a happy beginning but that does not make you who you are, it is the rest of it – who you choose to be', N'Soothsayer',N'Kung Fu Panda 2'),
  (N'Your mind is like this water, my friend. When it gets agitated, it becomes difficult to see. But if you allow it to settle, the answer becomes clear.', N'Master Oogway',N'Kung Fu Panda'),
  (N'You are the master of your destiny: No one and nothing can come in between you and your destiny except you. Take destiny by the horns and have fun.', N'Po',N'Kung Fu Panda'),
  (N'You don’t need to meditate for hours and hours to attain inner peace and enlightenment. You need only see, feel, and act from the heart. Let the heart guide you to your peaceful enlightenment.', N'Po',N'Kung Fu Panda 2'),
  (N'Don’t push past memories deeper inside of yourself. Let those memories breathe and let old wounds heal.', N'Po',N'Kung Fu Panda'),
  (N'There are no accidents.', N'Master Oogway',N'Kung Fu Panda'),
  (N'Once I realized the problem was not you but within me. I found inner peace and was able to harness the flow of the universe.', N'Master Shifu',N'Kung Fu Panda 2'),
  (N'To make something special you just have to believe it’s special.', N'Mr Ping',N'Kung Fu Panda'),
  (N'There is just news. There is no good or bad.', N'Master Oogway',N'Kung Fu Panda'),
  (N'Time is an illusion, there is only the now.', N'Master Shifu',N'Kung Fu Panda 3'),
  (N'Get ready to dance... with danger!', N'Panda Mei Mei',N'Kung Fu Panda 3'),
  (N'Your real strength comes from being the best you you can be. Who are you? What are you good at? What makes you, you?', N'Po',N'Kung Fu Panda 3'),
  (N'There is always something more to learn. Even for a master.', N'Master Oogway',N'Kung Fu Panda 3'),
  (N'If you only do what you can do, you’ll never be more than you are now.', N'Master Shifu',N'Kung Fu Panda 3');
GO
