#!/bin/sh
# usage : 
#   export STUDENTNUM=01
#   export RDSENDPOINT=immersionday.cowycczqvvny.eu-west-1.rds.amazonaws.com
#   curl https://gitlab.com/bsarda/webapp-sample-quotebank/-/raw/main/installer.sh | bash

echo "=== Starting installation ==="
# check if root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# set hostname
HSTNME="student${STUDENTNUM}-webtier"
hostnamectl set-hostname $HSTNME

# update
yum update -y
# force in case of not installed
yum install -y httpd

# stop apache
systemctl stop httpd
# clean folder
rm -rf /var/www/html/*
# download the file
curl -o /var/www/html/index.html https://gitlab.com/bsarda/webapp-sample-quotebank/-/raw/main/src-httpd/index.html
sed -i "s|localhost:3000|$(curl -s ipinfo.io/ip):8280|g" /var/www/html/index.html
# restart httpd
systemctl start httpd

# download app (go) binary
curl -o /bin/quotebank-app https://gitlab.com/bsarda/webapp-sample-quotebank/-/raw/main/quotebank-app
chmod +x /bin/quotebank-app
# trust for low ports (<1024)
setcap CAP_NET_BIND_SERVICE=+eip /usr/bin/quotebank-app

# set environment variables
cat << EOF > /etc/quotebank-vars
DB_TYPE=mssql
DB_HOST=$RDSENDPOINT
DB_PORT=1433
DB_NAME=quotebank
DB_TABLE=student$STUDENTNUM
DB_USER=admin
DB_PASS=BlueTea9!
SERVER_PORT=8280
EOF

# download the service definition and add it
curl -o /lib/systemd/system/quotebank-app.service https://gitlab.com/bsarda/webapp-sample-quotebank/-/raw/main/quotebank-app.service
systemctl start quotebank-app.service
systemctl enable quotebank-app.service

# validate
if [ "$(curl -sL -w '%{http_code}' http://localhost -o /dev/null)" != "200" ]; then
    echo "======================================================"
    echo "Installation failed. Please ask for help to the coach."
    echo '/!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\'
    exit
fi
if [ "$(curl -sL -w '%{http_code}' http://localhost:8280/api/ping -o /dev/null)" != "200" ]; then
    echo "======================================================"
    echo "Installation failed. Please ask for help to the coach."
    echo '/!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\'
    exit
fi

echo "=== Installation succeeded ! ==="