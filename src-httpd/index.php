<!DOCTYPE html>
<html>

  <head>
    <title>Test Page</title>
  </head>

  <style>
    header {
      display: flex;
      background-color: darkred;
      position: fixed;
      width: 100%;
      margin: 0;
      left: 0;
      top: 0;
      vertical-align: middle;
      height: 54px;
      align-items: center;
    }
    .logo {
      width: 32px;
      height: 32px;
      margin: 8px;
    }
    .page_title {
      color: white;
      font-size: 2rem;
      margin: auto;
    }
    article {
      margin-top: 64px;
      margin-bottom: 48px;
    }

    footer {
      background-color: LightCoral;
      position: fixed;
      width: 100%;
      left: 0;
      bottom: 0;
      vertical-align: middle;
      height: 32px;
    }
  </style>

  <body>
    <header>
      <img draggable=false alt="" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzQxLjEyIiBoZWlnaHQ9IjI5OS4zMjUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjIuNDg5IiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiPjxwYXRoIGZpbGw9IiNmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTI4OC4yMDMgMTE1LjUgMTg0LjQzNyA2My42MjMgNDEuNzggMTM0LjkwMWwxMDMuODA4IDUxLjg3N0wyODguMjAzIDExNS41eiIvPjxwYXRoIGZpbGw9IiNmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTI4OC4yMDMgMjI2LjgwM1YxMTUuNWwtMTQyLjYxNSA3MS4yNzhWMjk4LjA4bDE0Mi42MTUtNzEuMjc4eiIvPjxwYXRoIGZpbGw9IiNmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTE0NS41ODggMTg2Ljc3OCA0MS43ODEgMTM0Ljl2MTExLjMwM2wxMDMuODA3IDUxLjg3N1YxODYuNzc4eiIvPjxwYXRoIGZpbGw9IiNmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTQxLjg2NSAxMzUuMTEyIDEuMjQ1IDkxLjY3bDE0Mi42OTgtNzEuNDg5IDQwLjQ5NCA0My42MS0xNDIuNTcyIDcxLjMyeiIvPjxwYXRoIGZpbGw9IiNmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0ibTE4NC4yMjYgNjMuNjY1IDMxLjUxLTYyLjQyIDEwMy4yNiA1MS42NjUtMzEuNDI2IDYyLjM3OS0xMDMuMzQ0LTUxLjYyNHpNMTkuOTMgOTEuMTIybDIxLjcyNCA0NC4xMTYgMTAzLjI2IDUxLjYyNC0yMS42NC00NC4xMTZMMTkuOTMyIDkxLjEyMnoiLz48cGF0aCBmaWxsPSJub25lIiBkPSJNMTg0LjczMyA2My45MTh2MTA2LjIiLz48cGF0aCBmaWxsPSIjZmZmIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGQ9Im0xNDUuNzU3IDE4Ni4zNTYgNTEuMzM1IDcuMjU0IDE0Mi43ODMtNzEuMjc4LTUxLjQxOS03LjI1NC0xNDIuNjk5IDcxLjI3OGgweiIvPjwvZz48L3N2Zz4=" class="logo">
      <div class="page_title">Test Page</div>
    </header>
    <article>
      <p>
        This page is used to test the proper operation of the Apache HTTP server after it has been installed. If you can read
        this page, it means that the Apache HTTP server installed at this site is working properly.
      </p>
      <br/>
      <p>
        <?php echo "Server hostname is: ".gethostname(); ?>
        <br/>
        <?php echo "Server internal IP is: ".$_SERVER['SERVER_ADDR']; ?>
        <br/>
        <?php echo "Client IP is: ".$_SERVER['REMOTE_ADDR']; ?>
        <br/>
        <?php echo "Client host is: ".$_SERVER['REMOTE_HOST']; ?>
        <br/>
        <?php
          //set POST variables
          $url = 'http://ipinfo.io/ip';
          //open connection
          $ch = curl_init();
          //set options 
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          //execute post
          $result = curl_exec($ch);
          //close connection
          curl_close($ch);

          echo "Server external IP is: $result";
        ?>
      </p>
      <p>
        <?php print "Your client's machine IP address is ".$_SERVER['REMOTE_ADDR']; ?>
      </p>
    </article>
    <footer>
      Copyright 2022
    </footer>
  </body>
</html>